const mongoose = require('mongoose');

const { Schema } = mongoose;

// Create Schema
const AlbumSchema = new Schema({
  artist: {
    type: String,
    required: true,
  },
  albumID: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  released: {
    type: Number,
    required: true,
  },
  songLength: {
    type: Number,
    required: true,
  },
  cover: {
    url: {
      type: [String],
      required: true,
    },
  },
  listenTime: {
    type: Number,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('albums', AlbumSchema);
