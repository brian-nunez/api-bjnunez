const mongoose = require('mongoose');

const { Schema } = mongoose;

// Create Schema
const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
    required: false,
  },
  isBanned: {
    type: Boolean,
    default: false,
  },
  roles: {
    type: [String],
    default: [],
  },
  date: {
    type: Date,
    default: Date.now,
  },
});


module.exports = mongoose.model('users', UserSchema);
