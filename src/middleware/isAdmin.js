const ROLES = require('../utils/roles');

// eslint-disable-next-line consistent-return
async function isAdmin(req, res, next) {
  const { roles } = req.user;

  if (!(roles && Array.isArray(roles))) {
    return res.status(403).json({});
  }

  if (
    !(
      roles.includes(ROLES.ADMIN)
      || roles.includes(ROLES.ADMIN1)
      || roles.includes(ROLES.ADMIN2)
    )
  ) {
    return res.status(403).json({});
  }

  next();
}

module.exports = isAdmin;
