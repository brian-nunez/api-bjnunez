const User = require('../../models/User');

async function getUsers(req, res) {
  try {
    const users = await User.find({}, '-_id -password -__v');
    res.json(users);
  } catch (e) {
    res.status(500).json({});
  }
}

module.exports = getUsers;
