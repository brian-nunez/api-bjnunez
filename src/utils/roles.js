// In order of permissions
const roles = {
  ADMIN: 'ADMIN',
  ADMIN1: 'ADMIN_LEVEL_1',
  ADMIN2: 'ADMIN_LEVEL_2',
  MOD: 'MODERATOR_LEVEL_0',
  MOD1: 'MODERATOR_LEVEL_1',
  MOD2: 'MODERATOR_LEVEL_2',
  MOD3: 'MODERATOR_LEVEL_3',
  USER: 'USER',
};

module.exports = roles;
