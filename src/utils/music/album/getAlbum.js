const Album = require('../../../models/Album');

// eslint-disable-next-line consistent-return
async function validateGetAlbum(req, res, next) {
  const { artist, album } = req.query;

  if (
    !(
      artist && typeof artist === 'string'
      && album && typeof album === 'string'
    )
  ) return res.status(400).json({});

  next();
}

async function getAlbum(req, res) {
  const { artist, album } = req.query;

  try {
    const lookup = await Album.findOne({ artist, albumID: album }, '-_id -__v -date');
    if (!lookup) return res.status(404).json({});
    return res.json(lookup);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  getAlbum,
  validateGetAlbum,
};
