const Album = require('../../../models/Album');

async function createAlbumSearchQuery(req, res, next) {
  const { artist, album } = req.query;

  const searchQuery = {};

  if (artist && typeof artist === 'string') {
    searchQuery.artist = artist;
  }

  if (album && typeof album === 'string') {
    searchQuery.albumID = album;
  }

  req.searchQuery = searchQuery;

  next();
}

async function getAllAlbums(req, res) {
  const query = req.searchQuery;

  try {
    const lookup = await Album.find(query, '-_id -__v -date');

    const data = {
      data: lookup,
    };

    return res.json(data);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  createAlbumSearchQuery,
  getAllAlbums,
};
