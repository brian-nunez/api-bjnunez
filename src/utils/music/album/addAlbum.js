const Artist = require('../../../models/Artist');
const Album = require('../../../models/Album');

// eslint-disable-next-line consistent-return
async function validateAddAlbum(req, res, next) {
  if (!req.body) return res.status(400).json({});
  const {
    artist,
    albumID,
    name,
    released,
    songLength,
    cover,
    listenTime,
  } = req.body;

  const requiredStrings = [
    artist,
    albumID,
    name,
  ];

  // eslint-disable-next-line no-restricted-syntax
  for (const i of requiredStrings) {
    if (!(i && typeof i === 'string')) {
      return res.status(400).json({});
    }
  }

  if (!(released && typeof released === 'number')) {
    return res.status(400).json({});
  }

  if (!(songLength && typeof songLength === 'number')) {
    return res.status(400).json({});
  }
  if (!(cover && typeof cover === 'object')) {
    return res.status(400).json({});
  }
  if (!(cover.url && Array.isArray(cover.url))) {
    return res.status(400).json({});
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const url of cover.url) {
    if (!(url && typeof url === 'string')) {
      return res.status(400).json({});
    }
  }

  if (!(listenTime && typeof listenTime === 'number')) {
    return res.status(400).json({});
  }


  try {
    const artistLookup = await Artist.findOne({ handle: artist });
    if (!artistLookup) {
      return res.status(404).json({});
    }
    const lookup = await Album.findOne({ artist, albumID });
    if (lookup) {
      return res.status(409).json({});
    }
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }

  next();
}

async function addAlbum(req, res) {
  const {
    artist,
    albumID,
    name,
    released,
    songLength,
    cover,
    listenTime,
  } = req.body;

  try {
    const newAlbum = new Album({
      artist,
      albumID,
      name,
      released,
      songLength,
      cover,
      listenTime,
    });

    return newAlbum
      .save()
      .then((a) => res.json(a))
      .catch((err) => res.json({ err }));
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }
}

module.exports = {
  addAlbum,
  validateAddAlbum,
};
