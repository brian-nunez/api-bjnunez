const { getArtist, validateGetArtist } = require('./artist/getArtist');
const { createArtistSearchQuery, getAllArtists } = require('./artist/getAllArtists');
const { addArtist, validateAddArtist } = require('./artist/addArtist');

const { getAlbum, validateGetAlbum } = require('./album/getAlbum');
const { createAlbumSearchQuery, getAllAlbums } = require('./album/getAllAlbums');
const { addAlbum, validateAddAlbum } = require('./album/addAlbum');

const { getSong, validateGetSong } = require('./song/getSong');
const { createSongSearchQuery, getAllSongs } = require('./song/getAllSongs');
const { addSong, validateAddSong } = require('./song/addSong');

module.exports = {
  // Route /artist
  getArtist,
  validateGetArtist,
  addArtist,
  validateAddArtist,
  // Route /artist/all
  createArtistSearchQuery,
  getAllArtists,
  // Route /album
  getAlbum,
  validateGetAlbum,
  addAlbum,
  validateAddAlbum,
  // Route /album/all
  createAlbumSearchQuery,
  getAllAlbums,
  // Route /song
  getSong,
  validateGetSong,
  addSong,
  validateAddSong,
  // Route /artist/all
  createSongSearchQuery,
  getAllSongs,
};
