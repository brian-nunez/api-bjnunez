const Artist = require('../../../models/Artist');

// eslint-disable-next-line consistent-return
async function validateGetArtist(req, res, next) {
  const { handle } = req.query;
  console.log(handle);

  if (!handle) return res.status(400).json({});

  next();
}

async function getArtist(req, res) {
  const { handle } = req.query;
  console.log(handle);

  try {
    const lookup = await Artist.findOne({ handle }, '-_id name handle');
    if (!lookup) return res.status(404).json({});
    return res.json(lookup);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  getArtist,
  validateGetArtist,
};
