const Artist = require('../../../models/Artist');

// eslint-disable-next-line consistent-return
async function validateAddArtist(req, res, next) {
  if (!req.body) return res.status(400).json({});
  const { name, handle } = req.body;

  if (!(name && typeof name === 'string')) {
    return res.status(400).json({});
  }
  if (!(handle && typeof handle === 'string')) {
    return res.status(400).json({});
  }

  try {
    const lookup = await Artist.findOne({ handle });
    if (lookup) {
      return res.status(409).json({});
    }
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }

  next();
}

async function addArtist(req, res) {
  const { name, handle } = req.body;

  try {
    const newArtist = new Artist({
      name,
      handle,
    });

    return newArtist
      .save()
      .then((a) => res.json(a))
      .catch((err) => res.json({ err }));
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }
}

module.exports = {
  addArtist,
  validateAddArtist,
};
