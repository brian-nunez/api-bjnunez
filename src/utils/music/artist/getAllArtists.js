const Artist = require('../../../models/Artist');

async function createArtistSearchQuery(req, res, next) {
  const { handle } = req.query;

  const searchQuery = {};

  if (handle && typeof handle === 'string') {
    searchQuery.handle = handle;
  }

  req.searchQuery = searchQuery;

  next();
}

async function getAllArtists(req, res) {
  const query = req.searchQuery;

  try {
    const lookup = await Artist.find(query, '-_id name handle');

    const data = {
      data: lookup,
    };

    return res.json(data);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  createArtistSearchQuery,
  getAllArtists,
};
