const Artist = require('../../../models/Artist');
const Album = require('../../../models/Album');
const Song = require('../../../models/Song');

// eslint-disable-next-line consistent-return
async function validateAddSong(req, res, next) {
  if (!req.body) return res.status(400).json({});
  const {
    name,
    songID,
    length,
    artist,
    album,
    released,
    explicit,
    video,
    youtube,
  } = req.body;

  const requiredStrings = [
    name,
    songID,
    artist,
    album,
    name,
    youtube,
  ];
  const requiredNumbers = [
    length,
    released,
  ];

  // eslint-disable-next-line no-restricted-syntax
  for (const i of requiredStrings) {
    if (!(i && typeof i === 'string')) {
      return res.status(400).json({});
    }
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const i of requiredNumbers) {
    if (!(typeof i === 'number' && i >= 0)) {
      return res.status(400).json({});
    }
  }

  if (typeof explicit !== 'boolean') {
    return res.status(400).json({});
  }

  if (!(video && typeof video === 'object' && !Array.isArray(video))) {
    return res.status(400).json({});
  }

  if (
    !(video.url && Array.isArray(video.url))
    || !(video.thumbnail && Array.isArray(video.thumbnail))
  ) {
    return res.status(400).json({});
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const url of video.url) {
    if (!(url && typeof url === 'string')) {
      return res.status(400).json({});
    }
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const thumbnail of video.thumbnail) {
    if (!(thumbnail && typeof thumbnail === 'string')) {
      return res.status(400).json({});
    }
  }


  try {
    const artistLookup = await Artist.findOne({ handle: artist });
    if (!artistLookup) {
      return res.status(404).json({});
    }
    const albumLookup = await Album.findOne({ albumID: album });
    if (!albumLookup) {
      return res.status(404).json({});
    }
    const lookup = await Song.findOne({ songID });
    if (lookup) {
      return res.status(409).json({});
    }
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }

  next();
}

async function addSong(req, res) {
  const {
    name,
    songID,
    length,
    artist,
    album,
    released,
    explicit,
    video,
    youtube,
  } = req.body;

  try {
    const newSong = new Song({
      name,
      songID,
      length,
      artist,
      album,
      released,
      explicit,
      video,
      youtube,
    });

    return newSong
      .save()
      .then((a) => res.json(a))
      .catch((err) => res.json({ err }));
  } catch (e) {
    console.log(e);
    return res.status(500).json({});
  }
}

module.exports = {
  addSong,
  validateAddSong,
};
