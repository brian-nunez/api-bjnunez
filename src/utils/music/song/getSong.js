const Song = require('../../../models/Song');

// eslint-disable-next-line consistent-return
async function validateGetSong(req, res, next) {
  const { artist, album, song } = req.query;

  if (
    !(
      artist && typeof artist === 'string'
      && album && typeof album === 'string'
      && song && typeof song === 'string'
    )
  ) return res.status(400).json({});

  next();
}

async function getSong(req, res) {
  const { artist, album, song } = req.query;

  try {
    const lookup = await Song.findOne({ artist, album, songID: song }, '-_id -__v -date');
    if (!lookup) return res.status(404).json({});
    return res.json(lookup);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  getSong,
  validateGetSong,
};
