const Song = require('../../../models/Song');

async function createSongSearchQuery(req, res, next) {
  const { artist, album, song } = req.query;

  const searchQuery = {};

  if (artist && typeof artist === 'string') {
    searchQuery.artist = artist;
  }

  if (album && typeof album === 'string') {
    searchQuery.albumID = album;
  }

  if (song && typeof song === 'string') {
    searchQuery.songID = song;
  }

  req.searchQuery = searchQuery;

  next();
}

async function getAllSongs(req, res) {
  const query = req.searchQuery;

  try {
    const lookup = await Song.find(query, '-_id -__v -date');

    const data = {
      data: lookup,
    };

    return res.json(data);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

module.exports = {
  createSongSearchQuery,
  getAllSongs,
};
