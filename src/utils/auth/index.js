const {
  validateAuthRegister,
  authRegister,
} = require('./authRegister');

const {
  validateAuthLogin,
  authLogin,
} = require('./authLogin');

module.exports = {
  validateAuthRegister,
  authRegister,

  validateAuthLogin,
  authLogin,
};
