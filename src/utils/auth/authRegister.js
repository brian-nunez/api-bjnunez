const bcrypt = require('bcryptjs');

const User = require('../../models/User');
const roles = require('../roles');

// eslint-disable-next-line consistent-return
async function validateAuthRegister(req, res, next) {
  const {
    username,
    email,
    password,
    avatar,
  } = req.body;

  const requiredStrings = [
    username,
    email,
    password,
  ];

  // eslint-disable-next-line no-restricted-syntax
  for (const i of requiredStrings) {
    if (!(i && typeof i === 'string')) {
      return res.status(400).json({});
    }
  }

  if (avatar && typeof avatar !== 'string') {
    req.body.avatar = '';
  }

  next();
}

// eslint-disable-next-line consistent-return
async function authRegister(req, res) {
  const {
    username,
    email,
    password,
    avatar,
  } = req.body;

  try {
    const emailLookup = await User.findOne({ email });
    if (emailLookup) return res.status(409).json({ error: 'email' });
    const usernameLookup = await User.findOne({ username });
    if (usernameLookup) return res.status(409).json({ error: 'username' });

    const newUser = new User({
      username,
      email,
      password,
      avatar,
      isBanned: false,
      roles: [roles.USER],
    });
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // eslint-disable-next-line no-return-assign
    newUser.password = hashedPassword;
    // eslint-disable-next-line no-unreachable
    newUser
      .save()
      .then((user) => res.json(user))
      .catch(console.log);
  } catch (e) {
    return res.status(500).json({});
  }
}

module.exports = {
  validateAuthRegister,
  authRegister,
};
