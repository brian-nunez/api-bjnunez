const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../../models/User');

// eslint-disable-next-line consistent-return
async function validateAuthLogin(req, res, next) {
  const {
    username,
    password,
  } = req.body;

  if (
    !(username && typeof username === 'string')
    || !(password && typeof password === 'string')
  ) {
    return res.status(400).json({});
  }

  const userlookup = await User.findOne({ username });
  if (!userlookup) return res.status(404).json({});

  req.user = userlookup;

  next();
}

// eslint-disable-next-line consistent-return
async function authLogin(req, res) {
  const {
    username,
    password,
  } = req.body;

  try {
    const usernameLookup = await User.findOne({ username });
    if (!usernameLookup) return res.status(409).json({ error: 'username' });

    const isMatch = await bcrypt.compare(password, usernameLookup.password);
    if (isMatch) {
      const {
        id, username: name, isBanned, roles, avatar,
      } = usernameLookup;
      if (isBanned) return res.status(403).json({ banned: true });
      const payload = {
        id, username: name, isBanned, roles, avatar,
      };

      jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: 3600 },
        (err, token) => {
          res.json({
            success: true,
            token: `${token}`,
          });
        },
      );
    } else {
      return res.status(400).json({ error: 'password' });
    }
  } catch (e) {
    return res.status(500).json({});
  }
}

module.exports = {
  validateAuthLogin,
  authLogin,
};
