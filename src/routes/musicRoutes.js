const express = require('express');
const { auth, isAdmin } = require('../middleware');
const {
  getArtist,
  validateGetArtist,
  addArtist,
  validateAddArtist,
  createArtistSearchQuery,
  getAllArtists,

  getAlbum,
  validateGetAlbum,
  addAlbum,
  validateAddAlbum,
  createAlbumSearchQuery,
  getAllAlbums,

  getSong,
  validateGetSong,
  addSong,
  validateAddSong,
  createSongSearchQuery,
  getAllSongs,
} = require('../utils/music');

const musicRouter = express.Router();

function router() {
  // /artist
  musicRouter.get('/artist', validateGetArtist, getArtist);
  musicRouter.get('/artist/all', createArtistSearchQuery, getAllArtists);
  musicRouter.post('/artist', auth, isAdmin, validateAddArtist, addArtist);

  // /album
  musicRouter.get('/album', validateGetAlbum, getAlbum);
  musicRouter.get('/album/all', createAlbumSearchQuery, getAllAlbums);
  musicRouter.post('/album', auth, isAdmin, validateAddAlbum, addAlbum);

  // /song
  musicRouter.get('/song', validateGetSong, getSong);
  musicRouter.get('/song/all', createSongSearchQuery, getAllSongs);
  musicRouter.post('/song', auth, isAdmin, validateAddSong, addSong);
  return musicRouter;
}

module.exports = router;
