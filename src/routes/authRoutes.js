const express = require('express');

const authRouter = express.Router();

const {
  validateAuthRegister,
  authRegister,
  validateAuthLogin,
  authLogin,
} = require('../utils/auth');

function router() {
  authRouter.post('/login', validateAuthLogin, authLogin);
  authRouter.post('/register', validateAuthRegister, authRegister);

  return authRouter;
}

module.exports = router;
