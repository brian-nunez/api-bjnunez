const express = require('express');
const auth = require('../middleware/auth');
const isAdmin = require('../middleware/isAdmin');

const adminRouter = express.Router();

const {
  getUsers,
} = require('../utils/admin');

function router() {
  adminRouter.get('/users', auth, isAdmin, getUsers);

  return adminRouter;
}

module.exports = router;
