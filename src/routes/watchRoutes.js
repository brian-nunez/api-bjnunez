const express = require('express');

const watchRouter = express.Router();

function router() {
  watchRouter.route('/')
    .get(async (req, res) => {
      res.send({ msg: 'Watch Route /' });
    });

  watchRouter.route('/:vid')
    .all(async (req, res, next) => {
      next();
    })
    .get(async (req, res) => {
      res.send({ msg: 'Watch Route /:vid' });
    });
  return watchRouter;
}
module.exports = router;
