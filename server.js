const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const chalk = require('chalk');
const debug = require('debug')('app');
const morgan = require('morgan');
const bodyParser = require('body-parser');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config();
}

const app = express();
const PORT = process.env.PORT || 8080;

const connectDB = require('./src/utils/connectDB');

connectDB();

app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, './public/')));

app.set('views', './src/views');
app.set('view engine', 'ejs');

const watchRouter = require('./src/routes/watchRoutes')();
const musicRouter = require('./src/routes/musicRoutes')();
const authRouter = require('./src/routes/authRoutes')();
const adminRouter = require('./src/routes/adminRoutes')();

app.use('/watch', watchRouter);
app.use('/music', musicRouter);
app.use('/auth', authRouter);
app.use('/admin', adminRouter);

app.get('/', (req, res) => {
  res.render(
    'index',
    {
      title: 'Home',
    },
  );
});

app.listen(PORT, () => {
  debug(`Listening on port ${chalk.green(PORT)}`);
});
